import { useSelector } from 'react-redux';
import { selectIsAuthState } from '../../rootSelectors';
import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router';

export const useAuthRedirect = () => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const isAuth = useSelector(selectIsAuthState);

  useEffect(() => {
    !isAuth && pathname !== '/' && pathname !== '/forgot' && navigate('/login');
  }, [pathname, isAuth]);
};
