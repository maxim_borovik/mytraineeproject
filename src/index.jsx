import './index.css';
import React from 'react';
import { render } from 'react-dom';
import App from './App.jsx';
import { Provider } from 'react-redux';
import store from './redux';
import routeHistory from './utils/additionalyForRouting/routeHistory';
import CustomRouter from './utils/additionalyForRouting/customRouter.jsx';

render(
  <Provider store={store}>
    <CustomRouter history={routeHistory}>
      <App />
    </CustomRouter>
  </Provider>,
  document.getElementById('root'),
);
