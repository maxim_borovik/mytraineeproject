import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../saga';
import { authReducer } from '../pages/loginPage/reducer';
import { availableCoursesReducer } from '../pages/coursesPage/reducer';
import { forgotPasswordReducer } from '../pages/forgotPasswordPage/reducer';

const rootReducer = combineReducers({
  auth: authReducer,
  availableCourses: availableCoursesReducer,
  forgotPassword: forgotPasswordReducer,
});

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default store;
