export const primaryColors = {
  bgBlack: '#1F1F1F',
  colorTextWhite: '#FFFFFF',
  colorTextBlack: '#000000',
  grey: '#949494',
  bgGrey: '#E5E5E5',
  textGrey: '#B0B0B0',
};
