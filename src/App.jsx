import React from 'react';
import LoginPage from './pages/loginPage/LoginPage.jsx';
import { AppWrapper } from './components/AppWrapper.jsx';
import Header from './components/Header/Header.jsx';
import Main from './components/Main.jsx';
import HomePage from './pages/HomePage.jsx';
import { Route, Routes } from 'react-router-dom';
import ForgotPasswordPage from './pages/forgotPasswordPage/ForgotPasswordPage.jsx';
import CoursesPage from './pages/coursesPage/CoursesPage.jsx';
import { useAuthRedirect } from './utils/customHooks/useAuthRedirect';

const App = () => {
  useAuthRedirect();

  return (
    <AppWrapper>
      <Header />
      <Main>
        <Routes>
          <Route exact path='/' element={<HomePage />} />
          <Route path='/login' element={<LoginPage />} />
          <Route path='/forgot' element={<ForgotPasswordPage />} />
          <Route path='/main' element={<CoursesPage />} />
        </Routes>
      </Main>
    </AppWrapper>
  );
};

export default App;
