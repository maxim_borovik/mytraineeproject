import axios from 'axios';

export const http = axios.create({
  baseURL: 'https://university-api.techstack.dev',
  headers: {
    Authorization: localStorage.getItem('token'),
    'Content-Type': 'application/json',
  },
});
