import { http } from './index';

export const authAPI = {
  async login(data) {
    const response = await http.post('/authorization/login', { ...data });

    localStorage.setItem('token', response.data.data);

    return response.data;
  },

  async passwordRequest(data) {
    const resp = await http.post('/authorization/reset-password', { ...data });

    return resp.data.data;
  },

  async getAvailableCourses() {
    const resp = await http.get('/study-processes');

    return resp.data.data;
  },
};
