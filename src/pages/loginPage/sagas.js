import { call, put, takeLatest } from 'redux-saga/effects';
import { authAPI } from '../../api/api';
import { actionLoginError, actionLoginSuccess } from './actions';
import { LOG_IN } from './consts';
import routeHistory from '../../utils/additionalyForRouting/routeHistory';

function* workerLoginSaga({ payload }) {
  try {
    yield call(authAPI.login, payload);
    yield put(actionLoginSuccess());
    routeHistory.push('/main');
  } catch (error) {
    yield put(actionLoginError(error));
  }
}

export function* watchLoginSaga() {
  yield takeLatest(LOG_IN, workerLoginSaga);
}
