import { LOG_IN_ERROR, LOG_IN_SUCCESS } from './consts';
import { initialState } from './consts';

export function authReducer(state = initialState, action) {
  switch (action.type) {
    case LOG_IN_SUCCESS:
      return { ...state, isAuth: true };

    case LOG_IN_ERROR:
      return { ...state, error: action.payload };

    default:
      return state;
  }
}
