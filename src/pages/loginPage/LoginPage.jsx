import React from 'react';
import FormWrapper from '../../components/form/FormWrapper.jsx';
import { LogInForm } from '../../components/form/consts.js';
import { validateEmailPassword } from '../../components/form/validation/validEmailAndPassword';
import { useDispatch } from 'react-redux';
import { actionClickOnLogin } from './actions';

const LoginPage = () => {
  const dispatch = useDispatch();

  return (
    <FormWrapper
      tittle={LogInForm.tittle}
      inputArr={LogInForm.inputArrLogIn}
      buttonName={LogInForm.buttonName}
      callback={data => {
        dispatch(actionClickOnLogin(data));
      }}
      initialValues={LogInForm.initialValues}
      validate={validateEmailPassword}
    />
  );
};

export default LoginPage;
