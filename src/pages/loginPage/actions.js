import { LOG_IN, LOG_IN_ERROR, LOG_IN_SUCCESS } from './consts';

export const actionClickOnLogin = data => {
  return {
    type: LOG_IN,
    payload: data,
  };
};

export const actionLoginSuccess = () => {
  return {
    type: LOG_IN_SUCCESS,
  };
};

export const actionLoginError = error => {
  return {
    type: LOG_IN_ERROR,
    payload: error,
  };
};
