import React from 'react';
import { HomepageBlock } from '../components/HomepageBlock.jsx';

const HomePage = () => {
  return (
    <HomepageBlock>
      <div className='homepage-block'>
        Techstack <br />
        University
      </div>
    </HomepageBlock>
  );
};

export default HomePage;
