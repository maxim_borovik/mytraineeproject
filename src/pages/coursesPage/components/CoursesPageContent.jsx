import React from 'react';
import PropTypes from 'prop-types';
import { AVAILABLE_COURSE } from '../consts';
import CoursesCard from './courseCard/CoursesCard.jsx';

const CoursesPageContent = ({ selectedTab, searchedAvailableCourses, searchedFinishedCourses }) => {
  const courses = selectedTab === AVAILABLE_COURSE ? searchedAvailableCourses() : searchedFinishedCourses();

  return (
    <div className='main-content-block'>
      {courses.map(course => (
        <CoursesCard key={course.id} course={course} />
      ))}
    </div>
  );
};

CoursesPageContent.propTypes = {
  searchedFinishedCourses: PropTypes.func,
  searchedAvailableCourses: PropTypes.func,
  selectedTab: PropTypes.string,
};

export default CoursesPageContent;
