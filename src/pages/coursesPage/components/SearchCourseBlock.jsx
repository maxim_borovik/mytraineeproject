import React from 'react';
import PropTypes from 'prop-types';

const SearchCourseBlock = ({ query, checkAndSetQuery }) => {
  return (
    <div className='search-block'>
      <input
        value={query}
        type='text'
        placeholder='Search for the course'
        onChange={e => checkAndSetQuery(e.target.value)}
      />
    </div>
  );
};

SearchCourseBlock.propTypes = {
  query: PropTypes.string,
  checkAndSetQuery: PropTypes.func,
};

export default SearchCourseBlock;
