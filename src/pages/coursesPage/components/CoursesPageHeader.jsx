import React from 'react';
import TabBlock from './TabBlock.jsx';
import SearchCourseBlock from './SearchCourseBlock.jsx';
import PropTypes from 'prop-types';
import { AVAILABLE_COURSE, TABS } from '../consts';

const CoursesPageHeader = ({ query, checkAndSetQuery, archiveCourses, courses, selectedTab, setSelectedTab }) => {
  const countAllCourses = courses.length;
  const countFinishedCourses = archiveCourses.length;

  return (
    <div className='main-top-block'>
      <div className='tab-wrapper'>
        {TABS.map(tab => (
          <TabBlock key={tab} isSelectedTab={selectedTab === tab} onClick={() => setSelectedTab(tab)}>
            {tab}
            <div className='number-of-courses'>{tab === AVAILABLE_COURSE ? countAllCourses : countFinishedCourses}</div>
          </TabBlock>
        ))}
      </div>
      <SearchCourseBlock query={query} checkAndSetQuery={checkAndSetQuery} />
    </div>
  );
};

CoursesPageHeader.propTypes = {
  query: PropTypes.string,
  checkAndSetQuery: PropTypes.func,
  courses: PropTypes.array,
  archiveCourses: PropTypes.array,
  selectedTab: PropTypes.string,
  setSelectedTab: PropTypes.func,
};

export default CoursesPageHeader;
