import React from 'react';
import styled from 'styled-components';
import { primaryColors } from '../../../colors';

export const CoursesPageWrapper = styled.div`
  height: 100%;
  width: 100%;
  background-color: ${primaryColors.bgGrey};

  .main-top-block {
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    justify-content: space-between;
    background-color: ${primaryColors.bgBlack};
    height: 120px;
    padding: 0 100px;
  }

  .tab-wrapper {
    display: flex;
    flex-direction: row;
    width: content-box;
    justify-content: space-between;
  }

  .search-block {
    width: 397px;
    border-bottom: 1px solid ${primaryColors.textGrey};
    margin-bottom: 15px;

    input {
      color: ${primaryColors.colorTextWhite};
      width: 100%;
      outline: 0;
      font-size: 32px;
      outline-offset: 0;
      border: none;
      background-color: ${primaryColors.bgBlack};
    }
  }

  .main-content-block {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    padding-top: 8px;
    height: calc(100vh - 120px - 80px - 8px);
    width: 100%;
    overflow-y: scroll;
  }
`;

export default CoursesPageWrapper;
