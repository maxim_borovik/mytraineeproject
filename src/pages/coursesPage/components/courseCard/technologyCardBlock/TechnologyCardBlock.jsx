import React from 'react';
import TechnologyCardBlockWrapper from './TechnologyCardBlockWrapper.jsx';
import PropTypes from 'prop-types';

const TechnologyCardBlock = ({ course }) => {
  return (
    <TechnologyCardBlockWrapper>
      <div className='technologies'>
        {course.technologies.map(tech => (
          <span key={tech}>{tech}</span>
        ))}
      </div>
      <div>
        <div>Mentor:</div>
        {course.admins.map(admin => admin.studyAdminRole === 'Mentor' && <h4 key={admin.name}>{admin.name}</h4>)}
      </div>
    </TechnologyCardBlockWrapper>
  );
};

TechnologyCardBlock.propTypes = {
  course: PropTypes.object,
};

export default TechnologyCardBlock;
