import styled from 'styled-components';
import { primaryColors } from '../../../../../colors';

const TechnologyCardBlockWrapper = styled.div`
  margin: 0 24px 24px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  height: 40px;

  .technologies {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }

  span {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 111px;
    height: 40px;
    font-size: 20px;
    margin-right: 8px;
    color: ${primaryColors.colorTextWhite};
    background-color: ${primaryColors.bgBlack};
    border-radius: 6px;
  }

  h4 {
    margin: 0 5px 0 0;
    display: inline-block;
    height: 20px;
    font-size: 16px;
  }
`;

export default TechnologyCardBlockWrapper;
