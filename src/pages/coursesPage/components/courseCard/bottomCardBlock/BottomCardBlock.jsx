import React from 'react';
import Vector from '../../../../../img/Vector.svg';
import BottomCardBlockWrapper from './BottomCardBlockWrapper.jsx';

const BottomCardBlock = () => {
  return (
    <BottomCardBlockWrapper>
      <div className='bottom-card-content'>
        <span> Next:</span> State management intro <img src={Vector} />
      </div>
    </BottomCardBlockWrapper>
  );
};

export default BottomCardBlock;
