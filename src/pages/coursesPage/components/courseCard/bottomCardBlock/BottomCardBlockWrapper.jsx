import styled from 'styled-components';
import { primaryColors } from '../../../../../colors';

const BottomCardBlockWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  width: 100%;
  height: 100%;
  font-size: 32px;
  font-weight: 600;

  span {
    color: ${primaryColors.textGrey};
  }

  .bottom-card-content {
    cursor: pointer;
    margin-right: 24px;
  }
`;

export default BottomCardBlockWrapper;
