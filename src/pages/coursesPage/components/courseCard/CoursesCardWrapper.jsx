import styled from 'styled-components';
import { primaryColors } from '../../../../colors';

const CoursesCardWrapper = styled.div`
  display: flex;
  width: 1030px;
  min-height: 244px;
  flex-direction: column;
  align-items: center;
  color: ${primaryColors.colorTextBlack};
  background-color: ${primaryColors.colorTextWhite};
  margin-top: 16px;
  border-radius: 12px;
`;

export default CoursesCardWrapper;
