import React from 'react';
import PropTypes from 'prop-types';
import TopCardBlock from './topCardBlock/TopCardBlock.jsx';
import CoursesCardWrapper from './CoursesCardWrapper.jsx';
import BottomCardBlock from './bottomCardBlock/BottomCardBlock.jsx';

const CoursesCard = ({ course }) => {
  return (
    <CoursesCardWrapper>
      <TopCardBlock course={course} />
      <BottomCardBlock />
    </CoursesCardWrapper>
  );
};

CoursesCard.propTypes = {
  course: PropTypes.object,
};

export default CoursesCard;
