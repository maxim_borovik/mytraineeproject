import React from 'react';
import TopCardBlockWrapper from './TopCardBlockWrapper.jsx';
import TechnologyCardBlock from '../technologyCardBlock/TechnologyCardBlock.jsx';
import PropTypes from 'prop-types';

const TopCardBlock = ({ course }) => {
  return (
    <TopCardBlockWrapper>
      <div className='top-card-block-header'>
        <h2>{course.activeChapter.name}</h2>
        <span>{course.status.averageMark && course.status.averageMark.toFixed(1)}</span>
      </div>
      <div className='active-chapter'> Front-end foundation course</div>
      <TechnologyCardBlock course={course} />
    </TopCardBlockWrapper>
  );
};

TopCardBlock.propTypes = {
  course: PropTypes.object,
};

export default TopCardBlock;
