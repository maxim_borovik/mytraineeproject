import styled from 'styled-components';
import { primaryColors } from '../../../../../colors';

const TopCardBlockWrapper = styled.div`
  height: 167px;
  width: 100%;
  border-bottom: 1px solid ${primaryColors.bgBlack};

  .top-card-block-header {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    height: 36px;
    margin: 24px 24px 8px;

    span {
      color: ${primaryColors.colorTextWhite};
      display: flex;
      justify-content: center;
      align-items: center;
      width: 36px;
      height: 36px;
      border-radius: 50%;
      background-color: ${primaryColors.bgBlack};
      font-size: 16px;
    }
  }

  .active-chapter {
    margin: 0 0 16px 24px;
    font-size: 16px;
    font-weight: 400;
  }

  h2 {
    font-size: 32px;
    color: ${primaryColors.bgBlack};
  }
`;

export default TopCardBlockWrapper;
