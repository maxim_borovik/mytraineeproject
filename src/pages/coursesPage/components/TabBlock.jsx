import React from 'react';
import styled from 'styled-components';
import { primaryColors } from '../../../colors';
import PropTypes from 'prop-types';

const TabBlock = styled.button`
  position: relative;
  justify-content: center;
  align-items: center;
  width: content-box;
  height: 60px;
  font-size: 32px;
  border: none;
  outline: none;
  background-color: transparent;
  display: flex;
  color: ${props => (props.isSelectedTab ? primaryColors.colorTextWhite : primaryColors.textGrey)};
  border-bottom: 4px solid;
  margin-right: 56px;
  cursor: pointer;

  .number-of-courses {
    position: absolute;
    top: 10px;
    right: -9px;
    width: 9px;
    height: 20px;
    font-size: 16px;
  }
`;

TabBlock.propTypes = {
  state: PropTypes.bool,
};

export default TabBlock;
