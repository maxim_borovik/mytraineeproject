import { GETTING_COURSES_ERROR, SET_AVAILABLE_COURSES } from './consts';
import { initialState } from './consts';

export function availableCoursesReducer(state = initialState, action) {
  switch (action.type) {
    case SET_AVAILABLE_COURSES:
      return { ...state, availableCourses: action.payload };

    case GETTING_COURSES_ERROR:
      return { ...state, error: action.payload };

    default:
      return state;
  }
}
