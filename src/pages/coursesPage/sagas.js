import { call, put, takeLatest } from 'redux-saga/effects';
import { authAPI } from '../../api/api';
import { GET_AVAILABLE_COURSES } from './consts';
import { actionGettingCoursesError, actionSetAvailableCourses } from './actions';

function* workerAvailableCoursesSaga() {
  try {
    const courses = yield call(authAPI.getAvailableCourses);

    yield put(actionSetAvailableCourses(courses));
  } catch (error) {
    yield put(actionGettingCoursesError(error));
  }
}

export function* watchAvailableCoursesSaga() {
  yield takeLatest(GET_AVAILABLE_COURSES, workerAvailableCoursesSaga);
}
