export const initialState = {
  availableCourses: [],
  error: null,
};
export const GET_AVAILABLE_COURSES = 'GET_AVAILABLE_COURSES';
export const SET_AVAILABLE_COURSES = 'SET_AVAILABLE_COURSES';
export const GETTING_COURSES_ERROR = 'GETTING_COURSES_ERROR';

export const TABS = ['Available course', 'Archive'];
export const AVAILABLE_COURSE = 'Available course';
export const ARCHIVE = 'Archive';
