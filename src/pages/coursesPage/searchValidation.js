export const searchValidation = values => {
  let error = '';

  if (values.length > 10) {
    error = 'not more than 10 symbols';
  } else if (/[<~>]/.test(values)) {
    error = '"<",">","~" symbols are forbidden';
  }

  error && alert(error);

  return error;
};
