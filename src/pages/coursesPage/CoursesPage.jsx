import React, { useEffect, useState } from 'react';
import CoursesPageWrapper from './components/CoursesPageWrapper.jsx';
import CoursesPageHeader from './components/CoursesPageHeader.jsx';
import CoursesPageContent from './components/CoursesPageContent.jsx';
import { useDispatch, useSelector } from 'react-redux';
import { actionGetAvailableCourses } from './actions';
import { AVAILABLE_COURSE } from './consts';
import { searchValidation } from './searchValidation';
import { selectAvailableCourse } from './selectors';

const CoursesPage = () => {
  const courses = useSelector(selectAvailableCourse);
  const dispatch = useDispatch();
  const [selectedTab, setSelectedTab] = useState(AVAILABLE_COURSE);
  const [query, setQuery] = useState('');
  const archiveCourses = courses.filter(item => item.status.state !== 'Started');
  const checkAndSetQuery = value => {
    !searchValidation(value) && setQuery(value);
  };

  const searchedAvailableCourses = () => {
    return query ? courses.filter(({ activeChapter }) => activeChapter.name.toLowerCase().includes(query)) : courses;
  };

  const searchedFinishedCourses = () => {
    return query
      ? archiveCourses.filter(({ activeChapter }) => activeChapter.name.toLowerCase().includes(query))
      : archiveCourses;
  };

  useEffect(() => {
    dispatch(actionGetAvailableCourses());
  }, []);

  return (
    <CoursesPageWrapper>
      <CoursesPageHeader
        query={query}
        checkAndSetQuery={checkAndSetQuery}
        courses={courses}
        archiveCourses={archiveCourses}
        selectedTab={selectedTab}
        setSelectedTab={setSelectedTab}
      />
      <CoursesPageContent
        selectedTab={selectedTab}
        searchedAvailableCourses={searchedAvailableCourses}
        searchedFinishedCourses={searchedFinishedCourses}
      />
    </CoursesPageWrapper>
  );
};

export default CoursesPage;
