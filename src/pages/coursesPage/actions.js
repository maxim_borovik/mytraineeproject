import { GET_AVAILABLE_COURSES, GETTING_COURSES_ERROR, SET_AVAILABLE_COURSES } from './consts';

export const actionGetAvailableCourses = () => {
  return {
    type: GET_AVAILABLE_COURSES,
  };
};

export const actionSetAvailableCourses = courses => {
  return {
    type: SET_AVAILABLE_COURSES,
    payload: courses,
  };
};

export const actionGettingCoursesError = error => {
  return {
    type: GETTING_COURSES_ERROR,
    payload: error,
  };
};
