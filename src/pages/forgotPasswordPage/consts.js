export const GET_NEW_PASSWORD = 'GET_NEW_PASSWORD';
export const PASSWORD_SENT = 'PASSWORD_SENT';
export const GETTING_PASSWORD_ERROR = 'GETTING_PASSWORD_ERROR';

export const initialState = {
  newPassword: '',
  error: null,
};
