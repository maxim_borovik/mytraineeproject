export const selectNewPassword = state => state.forgotPassword.newPassword;
export const selectNewPasswordError = state => state.forgotPassword.error;
