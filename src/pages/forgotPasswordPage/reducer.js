import { GETTING_PASSWORD_ERROR, PASSWORD_SENT } from './consts';
import { initialState } from './consts';

export function forgotPasswordReducer(state = initialState, action) {
  switch (action.type) {
    case PASSWORD_SENT:
      return { ...state, newPassword: action.payload };

    case GETTING_PASSWORD_ERROR:
      return { ...state, error: action.payload };

    default:
      return state;
  }
}
