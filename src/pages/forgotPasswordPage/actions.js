import { GET_NEW_PASSWORD, GETTING_PASSWORD_ERROR, PASSWORD_SENT } from './consts';

export const actionGetNewPassword = data => {
  return {
    type: GET_NEW_PASSWORD,
    payload: data,
  };
};

export const actionSentPassword = data => {
  return {
    type: PASSWORD_SENT,
    payload: data,
  };
};

export const actionGettingPasswordError = error => {
  return {
    type: GETTING_PASSWORD_ERROR,
    payload: error,
  };
};
