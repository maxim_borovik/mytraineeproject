import { takeLatest, call, put } from 'redux-saga/effects';
import { authAPI } from '../../api/api';
import { GET_NEW_PASSWORD } from './consts';
import { actionGettingPasswordError, actionSentPassword } from './actions';

function* workerForgotPasswordSaga({ payload }) {
  try {
    const resp = yield call(authAPI.passwordRequest, payload);

    yield put(actionSentPassword(resp));
  } catch (error) {
    yield put(actionGettingPasswordError(error));
  }
}

export function* watchForgotPasswordSaga() {
  yield takeLatest(GET_NEW_PASSWORD, workerForgotPasswordSaga);
}
