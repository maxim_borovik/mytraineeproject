import React, { useEffect, useRef } from 'react';
import FormWrapper from '../../components/form/FormWrapper.jsx';
import { ForgotPasswordForm } from '../../components/form/consts.js';
import { validateEmail } from '../../components/form/validation/validEmail';
import { actionGetNewPassword } from './actions';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { selectNewPassword, selectNewPasswordError } from './selectors';
import { createStructuredSelector } from 'reselect';

const ForgotPasswordPage = () => {
  const dispatch = useDispatch();
  const { newPassword, error } = useSelector(
    createStructuredSelector({
      newPassword: selectNewPassword,
      error: selectNewPasswordError,
    }),
  );
  const navigate = useNavigate();
  const initialRender = useRef(true);

  useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false;
    } else if (newPassword) {
      confirm('password has been sent on your email');
      navigate('/main');
    } else {
      alert('something wrong, please try again');
    }
  }, [newPassword, error]);

  return (
    <FormWrapper
      tittle={ForgotPasswordForm.title}
      inputArr={ForgotPasswordForm.inputForgotArr}
      buttonName={ForgotPasswordForm.buttonName}
      callback={data => dispatch(actionGetNewPassword(data))}
      initialValues={ForgotPasswordForm.initialValues}
      validate={validateEmail}
    />
  );
};

export default ForgotPasswordPage;
