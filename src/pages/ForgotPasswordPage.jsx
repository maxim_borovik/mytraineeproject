import React from 'react';
import FormWrapper from '../components/form/FormWrapper.jsx';
import { ForgotPasswordForm } from '../components/form/consts.js';
import { validateEmail } from '../components/form/validation/validEmail';
import { authAPI } from '../api/api';

const ForgotPasswordPage = () => {
  return (
    <FormWrapper
      tittle={ForgotPasswordForm.title}
      inputArr={ForgotPasswordForm.inputForgotArr}
      buttonName={ForgotPasswordForm.buttonName}
      callback={data => authAPI.passwordRequest(data)}
      initialValues={ForgotPasswordForm.initialValues}
      validate={validateEmail}
    />
  );
};

export default ForgotPasswordPage;
