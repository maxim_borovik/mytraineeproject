export const LogInForm = {
  inputArrLogIn: [
    { name: 'email', type: 'text', label: 'Email', placeholder: null },
    { name: 'password', type: 'password', label: 'Password', placeholder: null },
  ],
  initialValues: {
    email: '',
    password: '',
  },
  tittle: 'Welcome back',
  buttonName: 'Sign in',
};

export const ForgotPasswordForm = {
  inputForgotArr: [{ name: 'email', type: 'text', label: 'Email', placeholder: null }],
  initialValues: {
    email: '',
  },
  title: 'Forgot your password?',
  buttonName: 'Reset password',
};
