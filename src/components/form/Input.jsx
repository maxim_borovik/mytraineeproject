import React from 'react';
import PropTypes from 'prop-types';
import InputWrapper from './InputWrapper.jsx';

const Input = ({ name, label, error, touched, ...attrs }) => {
  return (
    <InputWrapper>
      <div className='labels-wrapper'>
        {label && (
          <label className='input-label' htmlFor={name}>
            {label}
          </label>
        )}
      </div>
      <input name={name} {...attrs} />
      {touched && error && <span className='input-error'>{error}</span>}
    </InputWrapper>
  );
};

Input.propTypes = {
  name: PropTypes.string.isRequired,
  className: PropTypes.string,
  label: PropTypes.string,
  error: PropTypes.string,
  touched: PropTypes.bool,
};

export default Input;
