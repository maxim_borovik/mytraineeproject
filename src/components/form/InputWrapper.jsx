import React from 'react';
import styled from 'styled-components';
import { primaryColors } from '../../colors';

const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  color: ${primaryColors.colorTextWhite};
  margin-bottom: 42px;

  input {
    display: block;
    width: 100%;
    color: ${primaryColors.colorTextWhite};
    outline: 0;
    border: none;
    font-size: 24px;
    line-height: 30px;
    padding: 8px 0;
    border-bottom: solid 2px ${primaryColors.grey};
    background-color: ${primaryColors.bgBlack};
  }

  .labels-wrapper {
    display: flex;
    flex-direction: column;
    margin: 5px;
    height: 15px;
  }

  .input-label {
    top: 25px;
    font-size: 18px;
    line-height: 20px;
    color: ${primaryColors.grey};
  }

  .input-error {
    color: red;
  }
`;

export default InputWrapper;
