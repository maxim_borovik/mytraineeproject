import React from 'react';
import styled from 'styled-components';
import { primaryColors } from '../../colors';

export const SubmitButton = styled.button`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  min-height: 56px;
  margin-right: 100px;
  background-color: ${primaryColors.colorTextWhite};
  text-decoration: none;
  cursor: pointer;

  span {
    margin-left: 16px;
    color: ${primaryColors.colorTextBlack};
    font-size: 18px;
    line-height: 24px;
    font-weight: 400;
  }

  img {
    margin-right: 15px;
  }
`;
