import React from 'react';
import styled from 'styled-components';
import { primaryColors } from '../../colors';

const FormBlock = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 100px;
  width: 503px;
  color: ${primaryColors.colorTextWhite};

  h2 {
    font-size: 48px;
    line-height: 56px;
    margin-bottom: 48px;
  }

  a {
    display: block;
    color: ${primaryColors.colorTextWhite};
    text-decoration: none;
    font-size: 18px;
    margin-bottom: 48px;
  }
`;

export default FormBlock;
