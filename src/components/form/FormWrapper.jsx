import React from 'react';
import { useFormik } from 'formik';
import Input from './Input.jsx';
import PropTypes from 'prop-types';
import { SubmitButton } from './SubmitButton.jsx';
import vector from '../../img/Vector.svg';
import FormBlock from './FormBlock.jsx';
import { Link, useLocation } from 'react-router-dom';

const FormWrapper = ({ tittle, buttonName, inputArr, callback, initialValues, validate }) => {
  const { pathname } = useLocation();
  const isShowForgotPasswordLink = pathname === '/login';
  const formik = useFormik({
    initialValues,
    validate,
    onSubmit: e => {
      callback(e);
    },
  });

  return (
    <FormBlock>
      <form onSubmit={formik.handleSubmit}>
        <h2>{tittle}</h2>
        {inputArr.map(input => (
          <Input
            key={input.name}
            name={input.name}
            type={input.type}
            label={input.label}
            placeholder={input.placeholder}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            touched={formik.touched[input.name]}
            error={formik.errors[input.name]}
          />
        ))}
        {isShowForgotPasswordLink && <Link to='/forgot'> Forgot password? </Link>}
        <SubmitButton type='submit'>
          <span>{buttonName}</span>
          <img src={vector} />
        </SubmitButton>
      </form>
    </FormBlock>
  );
};

FormWrapper.propTypes = {
  tittle: PropTypes.string,
  buttonName: PropTypes.string,
  callback: PropTypes.func,
  inputArr: PropTypes.array,
  initialValues: PropTypes.object,
  validate: PropTypes.func,
};

export default FormWrapper;
