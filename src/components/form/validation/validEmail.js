export const validateEmail = values => {
  let errors = {};

  if (!values.email) {
    errors.email = 'Required';
  } else if (values.email.length > 50) {
    errors.email = 'not more than 50 symbols';
  } else if (/[<~>]/.test(values.email)) {
    errors.email = '"<",">","~" symbols are forbidden';
  } else if (!/^[\w\W]+@[a-z0-9]+\.[a-z]{2,3}$/.test(values.email)) {
    errors.email = 'invalid email format';
  }

  return errors;
};
