import { validateEmail } from './validEmail';

export const validateEmailPassword = values => {
  let errors = validateEmail(values);

  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length > 10) {
    errors.password = 'not more than 10 symbols!';
  } else if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W)[A-Za-z\d\W]{1,10}$/.test(values.password)) {
    errors.password = 'should contains lower/upper case letter, any number and any symbol';
  } else if (/[<~>]/.test(values.password)) {
    errors.password = '"<",">","~" symbols are forbidden';
  }

  return errors;
};
