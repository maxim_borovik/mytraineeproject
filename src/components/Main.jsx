import React from 'react';
import styled from 'styled-components';
import { primaryColors } from '../colors';

export const Main = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  height: 100%;
  width: 100%;
  color: ${primaryColors.colorTextWhite};
`;

export default Main;
