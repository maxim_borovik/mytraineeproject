import React from 'react';
import styled from 'styled-components';
import { primaryColors } from '../../colors';
import { BaseBlock } from '../BaseBlock.jsx';

export const HeaderContainer = styled(BaseBlock)`
  width: 100%;
  background-color: ${primaryColors.bgBlack};
  min-height: 72px;
  flex-direction: row;
  justify-content: space-between;

  .main-logo {
    margin-left: 100px;
    width: 124px;
    height: 24px;
  }
`;
