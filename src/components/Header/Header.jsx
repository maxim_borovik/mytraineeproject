import React from 'react';
import mainLogo from '../../img/light.svg';
import vector from '../../img/Vector.svg';
import { HeaderContainer } from './HeaderContainer.jsx';
import SignInButton from './SignInButton.jsx';
import { useLocation } from 'react-router-dom';

const Header = () => {
  const pathname = useLocation().pathname;
  const isShowSignInButton = pathname === '/';

  return (
    <HeaderContainer>
      <div className='main-logo'>
        <img src={mainLogo} />
      </div>
      {isShowSignInButton && (
        <SignInButton>
          <span>Sign in</span>
          <img src={vector} />
        </SignInButton>
      )}
    </HeaderContainer>
  );
};

export default Header;
