import React from 'react';
import styled from 'styled-components';
import { primaryColors } from '../../colors';
import { Link } from 'react-router-dom';

const SignInButton = styled(Link).attrs({ to: '/login' })`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 187px;
  height: 48px;
  margin-right: 100px;
  background-color: ${primaryColors.colorTextWhite};
  text-decoration: none;

  span {
    margin-left: 16px;
    color: ${primaryColors.colorTextBlack};
    font-size: 18px;
    line-height: 24px;
    font-weight: 400;
  }

  img {
    margin-right: 15px;
  }
`;

export default SignInButton;
