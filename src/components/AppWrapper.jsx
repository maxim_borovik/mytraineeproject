import styled from 'styled-components';
import { primaryColors } from '../colors.js';

export const AppWrapper = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  height: 100vh;
  width: 100%;
  background-color: ${primaryColors.bgBlack};
  overflow: hidden;
`;
