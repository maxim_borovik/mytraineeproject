import styled from 'styled-components';

export const BaseBlock = styled.div`
  box-sizing: content-box;
  display: flex;
  align-items: center;
`;
