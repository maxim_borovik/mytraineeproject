import styled from 'styled-components';
import { primaryColors } from '../colors';

export const HomepageBlock = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  width: 100%;
  background-color: ${primaryColors.bgBlack};

  .homepage-block {
    position: absolute;
    left: 99px;
    bottom: 71px;
    font-family: NHaasGroteskDSPro-Demi;
    font-weight: 400;
    font-size: 80px;
    line-height: 80px;
    color: ${primaryColors.colorTextWhite};
  }
`;
