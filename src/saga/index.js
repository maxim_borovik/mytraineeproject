import { watchLoginSaga } from '../pages/loginPage/sagas';
import { watchAvailableCoursesSaga } from '../pages/coursesPage/sagas';
import { spawn } from 'redux-saga/effects';
import { watchForgotPasswordSaga } from '../pages/forgotPasswordPage/sagas';

export default function* rootSaga() {
  yield spawn(watchLoginSaga);
  yield spawn(watchAvailableCoursesSaga);
  yield spawn(watchForgotPasswordSaga);
}
